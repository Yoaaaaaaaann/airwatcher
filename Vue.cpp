/*---------- Réalisation de la classe <Vue> (fichier Vue.cpp) ---------------*/

/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <ctime>
#include <iostream>
//------------------------------------------------------ Include personnel
#include "Vue.h"

using namespace std;

void Vue::afficherOptions()
// Mode d'emploi :
// Cette fonction permet d'afficher les options qui s'offrent à l'utilisateur
{
    cout<<"-------------------------------------------------------------------------------------------"<<endl;
    cout<<"Voici ce que vous pouvez faire avec cette application (seuls les services 3, 6 et 9 ont été codés)"<<endl;
    cout<<"Entrez le nombre correspondant au service désiré pour y acceder"<<endl;
    cout<<"Entrez x-i pour obtenir des informations sur le service n°x"<<endl;
    cout<<"1. Inscription"<<endl;
    cout<<"2. Connexion"<<endl;
    cout<<"3. Calcul de la qualité de l'air d'une zone donnée"<<endl;
    cout<<"4. Recherche des capteurs semblables à un capteur donné"<<endl;
    cout<<"5. Calcul de la qualité de l'air à des coordonnées données"<<endl;
    cout<<"6. Vérification de la fiabilité d'un capteur donné"<<endl;
    cout<<"7. Calcul de l'amélioration de l'air grâce à un AirCleaner donné"<<endl;
    cout<<"8. Calcul du rayon d'action d'un AirCleaner donné"<<endl;
    cout<<"9. Consulter les performances des algorithmes"<<endl;
    cout<<"10. Déterminer la fiabilité d'un particulier"<<endl;
    cout<<"11. Consulter mon total de points"<<endl;
    cout<<"12. Quitter l'application"<<endl;
    cout<<"-------------------------------------------------------------------------------------------"<<endl;

}
void Vue:: afficherMoyenneAir(int indiceATMO, double longi, double lati, double rayon, tm*date, int duree)
// Mode d'emploi :
// Cette fonction prend en parametre toutes les caractéristiques des mesures pour afficher le resultat à l'utilisateur
{
    
    if(indiceATMO!=0){ //Si le calcul a réussi
        tm * dateFin = new tm(); //On créé la date de fin  en prenant la date de début et en ajoutant la durée en heure
        dateFin->tm_mday = date->tm_mday;
        dateFin->tm_mon = date->tm_mon;
        dateFin->tm_year = date->tm_year;
        dateFin->tm_hour = date->tm_hour +duree;
        dateFin->tm_min = date->tm_min;
        dateFin->tm_sec = date->tm_sec;
        time_t tpsFin= mktime(dateFin);
        string strTpsDeb = asctime(date); //On convertit en string pour enelver le \n de fin (saut de ligne indésirable)
        string strTpsFin =asctime(localtime(&tpsFin));
        cout<<"La valeur de l'indice ATMO moyen aux coordonnées : "<<lati<<", "<<longi<<" entre le "<<strTpsDeb.substr(0,strTpsDeb.size()-1)<<" et le "<<strTpsFin.substr(0,strTpsFin.size()-1)<<" est de : "<<indiceATMO<<endl;
        cout<<"Une moyenne sur "<<rayon<<"km autour des coordonnées fournies a été faite"<<endl;
    }
    else{
        cout<<"Nous n'avons pas suffisamment de données pour établir une moyenne"<<endl;
    }
}
void Vue:: afficherFiabiliteCapteur(int indiceFiabilite, string idCapteur, tm*date, int duree)
// Mode d'emploi :
// Cette fonction prend en parametre toutes les caractéristiques des mesures pour afficher le resultat à l'utilisateur
{

    if(indiceFiabilite!=-1){ // Le la calcul a réussi
        tm * dateFin = new tm(); //On créé la date de fin  en prenant la date de début et en ajoutant la durée en heure
        dateFin->tm_mday = date->tm_mday;
        dateFin->tm_mon = date->tm_mon;
        dateFin->tm_year = date->tm_year;
        dateFin->tm_hour = date->tm_hour +duree;
        dateFin->tm_min = date->tm_min;
        dateFin->tm_sec = date->tm_sec;
        time_t tpsFin= mktime(dateFin);
        string strTpsDeb = asctime(date);
        string strTpsFin =asctime(localtime(&tpsFin));
        cout<<"Le capteur d'identifiant : "<<idCapteur<<" a un indice de fiabilité de "<<indiceFiabilite<<"/10"<<endl;
        cout<<"Les mesures ont été prélevées entre le "<<strTpsDeb.substr(0,strTpsDeb.size()-1)<<" et le "<<strTpsFin.substr(0,strTpsDeb.size()-1)<<endl;
    }
    else{
        cout<<"Nous n'avons pas suffisamment de données pour établir une moyenne"<<endl;
    }
    
}
void Vue::informationCalculMoyennAir()
// Mode d'emploi :
// Cette fonction affiche des informations supplémentaires sur la fonction CalculMoyenneAir
{
    cout<<"Cette fonction prend en paramètre :"<<endl;
    cout<<"- des coordonnées GPS (latitude et longitude) du centre de la zone"<<endl;
    cout<<"- le rayon de la zone (en km) "<<endl;
    cout<<"- la date du début des prises de mesure"<<endl;
    cout<<"- la durée (en nombre d'heure) pendant laquelle nous devons étudier les mesures"<<endl;
    cout<<"La fonction renvoie l'indice ATMO l'air dans cette zone"<<endl;

}
void Vue::informationFiabiliteCapteur()
// Mode d'emploi :
// Cette fonction affichier des informations supplémentaire sur la fonction FiabiliteCapteur
{
    cout<<"Cette fonction prend en paramètre :"<<endl;
    cout<<"- l'identifiant du capteur"<<endl;
    cout<<"- la date du début des prises de mesure"<<endl;
    cout<<"- la durée (en nombre d'heure) pendant laquelle nous devons étudier les mesures"<<endl;
    cout<<"La fonction renvoie un indice de fiabilité (une note sur 20), 20 étant la meilleure note"<<endl;
}
void Vue:: demanderGPS()
// Mode d'emploi :
// Cette fonction affiche un message de demande de coordonnées GPS
{
    cout<<"Entrez les coordonnées GPS de la zone sous la forme 'latitude-longitude' :"<<endl;
    cout<<"Entre 44 et 47.6 pour la latitude et entre -1 et 5.3 pour la longitude"<<endl;
}

void Vue:: demanderDate()
// Mode d'emploi :
// Cette fonction affiche un message de demande de Date
{
    cout<<"Entrez la date sous la forme'date-mois-annee(4 chiffres)-heure-minute-seconde' :"<<endl;
    cout<<"Les données ont été enregistrées entre le 1 janvier et le 31 décembre 2019"<<endl;
}
void Vue::demanderDuree()
// Mode d'emploi :
// Cette fonction affiche un message de demande de durée
{
    cout<<"Entrez la durée en heure :"<<endl;
}
void Vue:: demanderIdCapteur()
// Mode d'emploi :
// Cette fonction affiche un message de l'ID d'un capteur
{
    cout<<"Entrez l'identifiant du capteur sous le format : SensorX où X est le numéro du capteur"<<endl;
}
void Vue:: demanderRayon()
// Mode d'emploi :
// Cette fonction affiche un message de demande de rayon
{
    cout<<"Entrez le rayon en km"<<endl;
}
void Vue:: serviceNonDisponible()
// Mode d'emploi :
// Cette fonction affiche un message indiquant que le service choisi n'a pas été developpée
{
    cout<<"Veuillez nous excuser, ce service n'a pas été codé"<<endl;
}
void Vue:: afficherBienvenue()
// Mode d'emploi :
// Cette fonction permet d'affichier Bienvenue à l'utilisateur
{
    cout<<"Bienvenue dans notre application :)"<<endl;
}
void Vue:: quitterApp()
// Mode d'emploi :
// Cette fonction affiche un message d'au revoir
{
    cout<<"Nous sommes tristes de vous voir partir... A bientôt !"<<endl;
}

void Vue::afficherComplexite()
// Mode d'emploi :
// Cette fonction affiche les complexité de nos algorithmes
{
    cout<<"Complexité des algorithmes : "<<endl;
    cout<<"n=nb de mesures, p=nb de capteurs"<<endl;
    cout<<"ChercherMesureParCapteurEtType -> O(n)"<<endl;
    cout<<"ChercherCapteurZone -> O(p)"<<endl;
    cout<<"ConversionIndiceATMO -> O(1)"<<endl;
    cout<<"CalculMoyenneAir -> O(np)"<<endl;
    cout<<"DeterminerFiabiliteCapteur -> O(np)"<<endl;
}