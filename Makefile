params = -std=c++11
OBJ = Controleur.o Capteur.o Donnees.o Mesure.o AirCleaner.o Fournisseur.o Particulier.o Service.o Vue.o TypeDeMesure.o
EXE = airWatcher
SRC_DIR = Modele

$(EXE): $(OBJ)
	g++ -o $(EXE) $(OBJ) -g -pedantic $(params)

%.o: %.cpp 
	g++ -c $(params) $< -o $@

%.o: $(SRC_DIR)/%.cpp 
	g++ -c $(params) $< -o $@

clean:
	rm *.o
	rm $(EXE)
