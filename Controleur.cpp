/*---------- Réalisation de la classe <Controleur> (fichier Controleur.cpp) ---------------*/

/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <ctime>
#include <chrono>
#include <sstream>
#include <map>

//-------------------------------------------------------- Include personnelles
#include "Modele/Capteur.h"
#include "Modele/Fournisseur.h"
#include "Modele/AirCleaner.h"
#include "Modele/Particulier.h"
#include "Modele/Mesure.h"
#include "Modele/Donnees.h"
#include "Modele/Service.h"
#include "Controleur.h"
#include "Modele/TypeDeMesure.h"
#include "Vue.h"
using namespace std;


//from : https://www.techiedelight.com/determine-if-a-string-is-numeric-in-cpp/
bool isNumeric(string const &str)
//Fontion utilitaire pour vérifier qu'un string est numérique
{
    return !str.empty() && str.find_first_not_of("0123456789") == std::string::npos;
} 


double Controleur:: obtenirRayon()
//Mode d'emploi
// La fonction renvoie le rayon souhaité par l'utilisateur (en km)
{
   double rayon;
   string ray="";
   do
   {
      cin>>ray;
      if(isNumeric(ray)==false){
         cout<<"La valeur n'est pas numérique"<<endl;
      }
   } while (isNumeric(ray)==false); //tant que le rayon entré n'est pas numérique, on continue
   
   rayon = stod(ray);
   return rayon;
}

int Controleur:: obtenirDuree()
//Mode d'empploi
// La fonction renvoie un entier contenant la durée (en heure) d'étude des mesure souhaitée par l'utilisateur 
{
   int duree;
   string dur="";
   do
   {
      cin>>dur;
      if(isNumeric(dur)==false){
         cout<<"La valeur n'est pas numérique"<<endl;
      }
   } while (isNumeric(dur)==false); //Tant que la durée entrée n'est pas numérique, on continue
   duree = stod(dur);
   return duree;
}

tm* Controleur:: obtenirDate()
//Mode d'emploi :
//La fonction renvoie un tm* (une date) après l'avoir construit sur la base des informations de l'utilisateur
{
   tm* date = new tm();
   int scanOK =0;
   int jour,mois,annee,heure,minute,seconde;
   while(scanOK != 6){ //tant que nous n'avons pas nos 6 valeurs, on continue
      scanOK = scanf("%d-%d-%d-%d-%d-%d",&jour,&mois,&annee,&heure,&minute,&seconde);
      if(scanOK !=6){
         cout<<"Erreur de lecture, veuillez recommencer"<<endl;
      }
   }
   date->tm_mday = jour;
   date->tm_mon = mois-1; //les mois sont entre 0 et 11
   date->tm_year = annee-1900; // les années a partir de 1900
   date->tm_hour = heure;
   date->tm_min = minute;
   date->tm_sec = seconde;
   return date;
}

int Controleur::obtenirOption(bool& infos)
//Mode d'emploi :
// La fonction obtenirOption renvoie le choix de l'utilsateur sur le service qu'il desire utiliser.
// La paramètre bool ici permet de savoir si l'utilisateur veut obtenir des informations supplémentaire sur la fonction
{
   string in;
   int choix;
   bool chiffres2 = false; //s'il y a deux chiffres dans le nombre choisi
   do{
      cin >> in;
      if(isNumeric(in.substr(0,1))){
         if(isNumeric(in.substr(0,2))){
            chiffres2 = true;
            choix = stoi(in.substr(0,2));
         } else{
            choix = stoi(in.substr(0,1));
         }
      }
      else{
         choix = 0;
      } 
   } while(choix<1 || choix >12);
   if((chiffres2==false && (in.length()>2) && in.substr(1,2)=="-i") || (chiffres2==true && (in.length()>3) && in.substr(2,2)=="-i"))
   {
      infos = true; //si on veut des infos
   }
   return choix;
}

void Controleur::obtenirGPS(double& latitude, double& longitude)
//Mode d'emploi :
// La fonction prend en paramètre deux doubles qui contiendront la longitude et la latitude des coordonnées choisies par l'utilisateur
{
   int ok = 0;
   do{
      ok = scanf("%lf-%lf",&latitude,&longitude);
      if(ok!=2){
         cout<<"Erreur de lecture, veuillez recommencer"<<endl;
      }
   } while (ok!=2); //tant que nous n'avons pas nos deux coordonéees
}

string Controleur::obtenirIdCapteur(map<string, Capteur*>* listeCapteur)
//Mode d'emploi 
// La fonction renvoie l'ID du capteur souhaité par l'utilisateur. Elle prend en paramètre une liste de capteur afin de vérifier que 
//le capteur choisi appartient bien a notre liste de capteur.
{
   bool present;
   string id;
   do{
      cin >> id;
      present = (listeCapteur->find(id) != listeCapteur->end());
      if(!present){
         cout<<"La capteur choisi n'appartient pas à notre base de données"<<endl;
      }
   } while (! present); //Tant que le capteur selectionné n'est pas dans notre liste de capteur
   return id;
}

int Controleur::mainTest()
//Mode d'emploi :
// La fonction mainTest est la fonction appelée si nous voulons faire des tests. Elle lance une batterie de tests en live afin
// montrer le bon fonctionnement de nos services
{
   enum IndexCouleur { NOIR, ROUGE, VERT, JAUNE, BLEU, MAGENTA, CYAN, BLANC, RESET };
   static const char * const COULEUR [ ] = { //CREATION DES COULEURS POUR LES TESTS
   "\033[30m", /* Noir */ "\033[31m", /* Rouge */ "\033[32m", /* Vert */
   "\033[33m", /* Jaune */ "\033[34m", /* Bleu */ "\033[35m", /* Magenta */
   "\033[36m", /* Cyan */ "\033[37m", /* Blanc */ "\033[m\017" /* Reset */ };

   /* creation de la vue, des données et sauvegarde de nos données de tests */
   Vue aff=Vue();
   Donnees* data =new Donnees();
   data->sauvegarderDonnees("jeuDeDonnees/testsensors.csv");
   data->sauvegarderDonnees("jeuDeDonnees/testmeasurements.csv");
   Service* service= new Service(data);
   /*************************************************************************/
   bool testPass =false;
   int nbTestPass=0;
   int nbTest=0;
   /**************1er test**************************************/
   cout<<"Bienvenue dans notre service de test"<<endl;
   cout<<"////////////////////////////////////////////////////////////////////////////"<<endl;
   cout<<"Test de la fonction chercherCapteurZone(double latitude, double longitude, double rayon)"<<endl;
   cout<<"-------------------------Test 1------------------------"<<endl;
   cout<<"latitude = 43;  longitude = -5; rayon = 50;"<<endl;
   cout<<"Sortie attendue : vector -> {}"<<endl;
   double latitude = 43;
   double longitude = -5;
   double rayon =50;
   auto start = std::chrono::high_resolution_clock::now(); //demarage du chrono
   auto capteurs = service->ChercherCapteurZone(longitude,latitude, rayon);
   auto end = std::chrono::high_resolution_clock::now(); // fin du chrono
   auto int_s = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
   cout<<"Sortie : vector -> ";
   cout<<"{"; //affichage des resultats
   for(int i=0; i<capteurs.size(); i++){
      cout<<"'"<<capteurs[i]<<"'";
   }
   cout<<"}"<<endl;
   cout<<"Temps mis : "<<int_s.count()<<"µs"<<endl;
   testPass =false;
   if(capteurs.size()==0) {
      testPass=true;
      nbTestPass++;
   }
   nbTest++;
   testPass? (cout<<COULEUR[VERT]<<"***************SUCCESS*******************"<<COULEUR[RESET]<<endl) : (cout<<COULEUR[ROUGE]<<"***************FAIL*******************"<<COULEUR[RESET]<<endl) ;
   
   /**************2eme test**************************************/
   cout<<endl<<"-----------------------Test 2---------------------"<<endl;
   cout<<"latitude = 44;  longitude = -0.3; rayon = 100;"<<endl;
   cout<<"Sortie attendue : vector -> {'Sensor0','Sensor1', 'Sensor4'}"<<endl;
   auto vectAttendu = {"Sensor0", "Sensor1", "Sensor4"};
   latitude = 44;
   longitude = -0.3;
   rayon =100;
   start = std::chrono::high_resolution_clock::now();
   capteurs = service->ChercherCapteurZone(longitude,latitude, rayon);
   end = std::chrono::high_resolution_clock::now();
   int_s = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
   cout<<"Sortie : vector -> ";
   cout<<"{"; //affichage des résultats
   for(int i=0; i<capteurs.size(); i++){
      cout<<"'"<<capteurs[i]<<"'";
   }
   cout<<"}"<<endl;
   cout<<"Temps mis : "<<int_s.count()<<"µs"<<endl;
   testPass =false;
   if(equal(vectAttendu.begin(), vectAttendu.end(), capteurs.begin())) {
      testPass=true;
      nbTestPass++;
   }
   nbTest++;
   testPass? (cout<<COULEUR[VERT]<<"***************SUCCESS*******************"<<COULEUR[RESET]<<endl) : (cout<<COULEUR[ROUGE]<<"***************FAIL*******************"<<COULEUR[RESET]<<endl) ;
   
   /**************3eme test**************************************/
   cout<<"////////////////////////////////////////////////////////////////"<<endl;
   cout<<"Test de la fonction ConversionIndiceATMO( string polluant, double concentration)"<<endl;
   cout<<"----------------------------------Test 1-----------------------------------"<<endl;
   cout<<"polluant = 'O3';  concentration = 50.75"<<endl;
   cout<<"Sortie attendue : 2"<<endl;
   string polluant ="O3";
   double concentration= 50.75;
   start = std::chrono::high_resolution_clock::now();
   int indice = service->ConversionIndiceATMO(polluant, concentration);
   end = std::chrono::high_resolution_clock::now();
   int_s = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
   cout<<"Sortie : "<<indice<<endl;
   cout<<"Temps mis :"<<int_s.count()<<"µs"<<endl;
   testPass =false;
   if(indice == 2) {
      testPass=true;
      nbTestPass++;
   }
   nbTest++;
   testPass? (cout<<COULEUR[VERT]<<"***************SUCCESS*******************"<<COULEUR[RESET]<<endl) : (cout<<COULEUR[ROUGE]<<"***************FAIL*******************"<<COULEUR[RESET]<<endl) ;
   
   /**************4eme test**************************************/
   cout<<"---------------------------------Test 2----------------------------"<<endl;
   cout<<"polluant = 'PM10';  concentration = 99.12"<<endl;
   cout<<"Sortie attendue : 10"<<endl;
   polluant ="PM10";
   concentration= 99.12;
   start = std::chrono::high_resolution_clock::now();
   indice = service->ConversionIndiceATMO(polluant, concentration);
   end = std::chrono::high_resolution_clock::now();
   int_s = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
   cout<<"Sortie : "<<indice<<endl;
   cout<<"Temps mis :"<<int_s.count()<<"µs"<<endl;
   testPass =false;
   if(indice == 10) {
      testPass=true;
      nbTestPass++;
   }
   nbTest++;
   testPass? (cout<<COULEUR[VERT]<<"***************SUCCESS*******************"<<COULEUR[RESET]<<endl) : (cout<<COULEUR[ROUGE]<<"***************FAIL*******************"<<COULEUR[RESET]<<endl) ;
   
   /**************5eme test**************************************/
   cout<<"////////////////////////////////////////////////////////////////"<<endl;
   cout<<"Test de la fonction ChercherMesureParCapteurEtType(string idcapteur, string polluant)"<<endl;
   cout<<"----------------------------------Test 1-----------------------------------"<<endl;
   cout<<"idcapteur = 'Sensor2';  polluant = 'O3'"<<endl;
   cout<<"Sortie attendue : vector -> {'Mesure8'}"<<endl;
   auto sortieTh = {"Mesure8"};
   polluant = "O3";
   string idcapteur = "Sensor2";
   start = std::chrono::high_resolution_clock::now();
   auto mesures = service->ChercherMesureParCapteurEtType(idcapteur,polluant);
   end = std::chrono::high_resolution_clock::now();
   int_s = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
   cout<<"Sortie : vector -> ";
   cout<<"{";
   for(int i=0; i<mesures.size(); i++){
      cout<<"'"<<mesures[i]->getId()<<"'";
   }
   cout<<"}"<<endl;
   cout<<"Temps mis : "<<int_s.count()<<"µs"<<endl;
   testPass =false;
   if(mesures[0]->getId() == "Mesure8") {
      testPass=true;
      nbTestPass++;
   }
   nbTest++;
   testPass? (cout<<COULEUR[VERT]<<"***************SUCCESS*******************"<<COULEUR[RESET]<<endl) : (cout<<COULEUR[ROUGE]<<"***************FAIL*******************"<<COULEUR[RESET]<<endl) ;
   
   /**************6eme test**************************************/
   cout<<"----------------------------------Test 2-----------------------------------"<<endl;
   cout<<"idcapteur = 'Sensor2';  polluant = 'CO2'"<<endl;
   cout<<"Sortie attendue : vector -> {}"<<endl;
   polluant = "CO2";
   idcapteur = "Sensor2";
   start = std::chrono::high_resolution_clock::now();
   mesures = service->ChercherMesureParCapteurEtType(idcapteur,polluant);
   end = std::chrono::high_resolution_clock::now();
   int_s = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
   cout<<"Sortie : vector -> ";
   cout<<"{";
   for(int i=0; i<mesures.size(); i++){
      cout<<"'"<<mesures[i]->getId()<<"'";
   }
   cout<<"}"<<endl;
   cout<<"Temps mis : "<<int_s.count()<<"µs"<<endl;
   testPass =false;
   if(mesures.size()==0) {
      testPass=true;
      nbTestPass++;
   }
   nbTest++;
   testPass? (cout<<COULEUR[VERT]<<"***************SUCCESS*******************"<<COULEUR[RESET]<<endl) : (cout<<COULEUR[ROUGE]<<"***************FAIL*******************"<<COULEUR[RESET]<<endl) ;
   
   /**************7eme test**************************************/
   cout<<"///////////////////////////////////////////////////////////////"<<endl;
   cout<<"Test de la fonction CalculerMoyenneAir(double lat, double long, double Rayon, Date DateHeure,  double Duree)"<<endl;
   cout<<"---------------------------Test 1----------------------------------"<<endl;
   cout<<"Pour lat = 43.0, long = -5.0, rayon = 50, date = 2019-01-01 12:00:00, duree =  24"<<endl;
   cout<<"Sortie attendue : 0 (il n'y a pas assez de données pour faire les calculs)"<<endl;
   latitude = 43;
   longitude = -5;
   rayon =50;
   int duree =24;
   tm * date = new tm();
   date->tm_mday = 1;
   date->tm_mon = 0;
   date->tm_year = 2019 - 1900;
   date->tm_hour = 12;
   date->tm_min = 0;
   date->tm_sec = 0;
   start = std::chrono::high_resolution_clock::now();
   indice = service->CalculMoyenneAir(longitude, latitude, rayon, date,duree);
   end = std::chrono::high_resolution_clock::now();
   int_s = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
   cout<<"Sortie : "<<indice<<endl;
   cout<<"Temps mis :"<<int_s.count()<<"µs"<<endl;
   testPass =false;
   if(indice ==0) {
      testPass=true;
      nbTestPass++;
   }
   nbTest++;
   testPass? (cout<<COULEUR[VERT]<<"***************SUCCESS*******************"<<COULEUR[RESET]<<endl) : (cout<<COULEUR[ROUGE]<<"***************FAIL*******************"<<COULEUR[RESET]<<endl) ;
   
   /**************8eme test**************************************/
   cout<<"--------------------------Test 2 ------------------------"<<endl;
   cout<<"Pour lat = 44.0, long = -0.3, rayon = 100, date = 2019-01-01 12:00:00, duree = 24"<<endl;
   cout<<"Sortie attendue : 7"<<endl;
   latitude = 44;
   longitude = -0.3;
   rayon =100;
   start = std::chrono::high_resolution_clock::now();
   indice = service->CalculMoyenneAir(longitude, latitude, rayon, date,duree);
   end = std::chrono::high_resolution_clock::now();
   int_s = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
   cout<<"Sortie : "<<indice<<endl;
   cout<<"Temps mis :"<<int_s.count()<<"µs"<<endl;
   testPass =false;
   if(indice ==7) {
      testPass=true;
      nbTestPass++;
   }
   nbTest++;
   testPass? (cout<<COULEUR[VERT]<<"***************SUCCESS*******************"<<COULEUR[RESET]<<endl) : (cout<<COULEUR[ROUGE]<<"***************FAIL*******************"<<COULEUR[RESET]<<endl) ;
   
   /**************9eme test**************************************/
   cout<<"////////////////////////////////////////////////////////////////"<<endl;
   cout<<"Test de la fonction DeterminerFiabiliteCapteur(string idcapteur, tm * dateDeb, int duree)"<<endl;
   cout<<"----------------------------------Test 1-----------------------------------"<<endl;
   cout<<"idcapteur = 'Sensor0';  dateDeb = '01-01-2019-12-00-00';  duree = '24'"<<endl;
   cout<<"Sortie attendue : '6'"<<endl;
   idcapteur = "Sensor0";
   tm* dateDeb = new tm();
   dateDeb->tm_hour = 12;
   dateDeb->tm_year = 2019-1900;
   dateDeb->tm_mday = 1;
   dateDeb->tm_mon = 0;
   dateDeb->tm_min = 0;
   dateDeb->tm_sec = 0;
   duree = 24;
   start = std::chrono::high_resolution_clock::now();
   int fiabilite = service->DeterminerFiabiliteCapteur(idcapteur,dateDeb,duree);
   end = std::chrono::high_resolution_clock::now();
   int_s = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
   cout<<"Sortie : '"<<fiabilite<<"'"<<endl;
   cout<<"Temps mis : "<<int_s.count()<<"µs"<<endl;
   testPass =false;
   if(fiabilite ==6) {
      testPass=true;
      nbTestPass++;
   }
   nbTest++;
   testPass? (cout<<COULEUR[VERT]<<"***************SUCCESS*******************"<<COULEUR[RESET]<<endl) : (cout<<COULEUR[ROUGE]<<"***************FAIL*******************"<<COULEUR[RESET]<<endl) ;
   
   /**************10eme test**************************************/
   cout<<"----------------------------------Test 2-----------------------------------"<<endl;
   cout<<"idcapteur = 'Sensor0';  dateDeb = '03-01-2019-12-00-00';  duree = '24'"<<endl;
   cout<<"Sortie attendue : '-1'"<<endl;
   idcapteur = "Sensor0";
   dateDeb->tm_hour = 12;
   dateDeb->tm_year = 2019-1900;
   dateDeb->tm_mday = 3;
   dateDeb->tm_mon = 0;
   dateDeb->tm_min = 0;
   dateDeb->tm_sec = 0;
   duree = 24;
   start = std::chrono::high_resolution_clock::now();
   fiabilite = service->DeterminerFiabiliteCapteur(idcapteur,dateDeb,duree);
   end = std::chrono::high_resolution_clock::now();
   int_s = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
   cout<<"Sortie : '"<<fiabilite<<"'"<<endl;
   cout<<"Temps mis : "<<int_s.count()<<"µs"<<endl;
   testPass =false;
   if(fiabilite ==-1) {
      testPass=true;
      nbTestPass++;
   }
   nbTest++;
   testPass? (cout<<COULEUR[VERT]<<"***************SUCCESS*******************"<<COULEUR[RESET]<<endl) : (cout<<COULEUR[ROUGE]<<"***************FAIL*******************"<<COULEUR[RESET]<<endl) ;
   cout<<"/////////////////////////////////////////////"<<endl;
   cout<<COULEUR[CYAN]<<"//////////// BILAN DES TESTS /////////////////"<<COULEUR[RESET]<<endl;

   cout<<COULEUR[VERT]<<"TESTS VALIDES : "<<((double)nbTestPass/(double)nbTest)*100<<"%"<<COULEUR[RESET]<<endl;
   cout<<COULEUR[ROUGE]<<"TEST NON VALIDES : "<< (1- (double)nbTestPass/(double)nbTest)*100<<"%"<<COULEUR[RESET]<<endl;
   return 0;
}

int Controleur :: main()
//Mode d'emploi :
// La fonction main est la fonction qui se lance lors du demarrage de notre programme (si nous ne voulons pas voir les tests)
// Il s'agit de l'interface utilisateur en ligne de commande.
{
   /*Creation de la vue, des service, des données et enregistrement de la base de données */
   Vue aff = Vue();
   Donnees* data = new Donnees();
   data->sauvegarderDonnees("jeuDeDonnees/sensors.csv");
   data->sauvegarderDonnees("jeuDeDonnees/measurements.csv");
   data->sauvegarderDonnees("jeuDeDonnees/users.csv");
   data->sauvegarderDonnees("jeuDeDonnees/providers.csv");
   data->sauvegarderDonnees("jeuDeDonnees/attributes.csv");
   Service* service= new Service(data);
   /****************************************************************************/
   aff.afficherBienvenue();
   while(1){ //Boucle infinie
      aff.afficherOptions();
      bool info =false;
      int option = obtenirOption(info);
      switch (option)
      {
      case 1:
         aff.serviceNonDisponible();
         break;
      case 2:
         aff.serviceNonDisponible();
         break;

      case 3: //Calcul moyenne air
         if(info){ //on veut les infos
            aff.informationCalculMoyennAir();
         }
         else{
            double latitude, longitude, rayon;
            tm* date;
            int duree;
            /*Demande des différents parametre à l'utilisateur */
            aff.demanderGPS();
            obtenirGPS(latitude, longitude);
            aff.demanderDate();
            date = obtenirDate();
            aff.demanderDuree();
            duree = obtenirDuree();
            aff.demanderRayon();
            rayon = obtenirRayon();
            /**************************************************/
            /* Calcul de la moyenne par le service */
            int moyAir = service->CalculMoyenneAir(longitude, latitude, rayon, date, duree);
            /*Calcul de la moyenne */
            aff.afficherMoyenneAir(moyAir, longitude, latitude, rayon, date, duree);        
         }
         break;
      case 4:
         aff.serviceNonDisponible();
         break;
      case 5:
         aff.serviceNonDisponible();
         break;

      case 6: // Calculer fiabilité des capteurs
         if(info){ //On veut des infos
            aff.informationFiabiliteCapteur();
         }
         else{
            string idCap;
            tm* date;
            int duree;
            //Demande des paramètres
            aff.demanderIdCapteur();
            idCap = obtenirIdCapteur(data->getListeCapteur());
            aff.demanderDate();
            date = obtenirDate();
            aff.demanderDuree();
            duree = obtenirDuree();
            //Calcul de la fiabilité
            int fiabilite = service->DeterminerFiabiliteCapteur(idCap, date, duree);
            //Affichage de la fiabilité
            aff.afficherFiabiliteCapteur(fiabilite, idCap, date, duree);
         }
         break;
      case 7:
         aff.serviceNonDisponible();
         break;
      case 8:
         aff.serviceNonDisponible();
         break;
      case 9: //Affichage de la complexité
         aff.afficherComplexite();
         break;
      case 10:
         aff.serviceNonDisponible();
         break;
      case 11:
         aff.serviceNonDisponible();
         break;
      case 12:
         aff.quitterApp();
         return(0);
      default:
         aff.serviceNonDisponible();
         break;
      }
   }
   
   return 0;
}

int main(int nbArg, char**args)
{
   //On créé notre controleur
   Controleur c =Controleur();
   int retour;
   if(nbArg>=2){
      if(strcmp(args[1],"test")==0){ //Si on veut lancer les tests
         retour =c.mainTest();
      }
   }
   else{
     retour= c.main();
   }
   return retour;
}
