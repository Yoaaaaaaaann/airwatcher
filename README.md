## Bienvenue sur l'application AirWatcher !
L'application a été réalisé dans le cadre d'un TP de C++

Air Watcher est une application en ligne de commande qui, reposant sur une base de donnée de fichiers .csv, est capable d'analyser la qualité de l'air à une date et un lieu donné.
L'application est aussi capable de déterminer la fiabilité d'un capteur.

### Exécuter l'application airWatcher

Pour exécuter notre application, veuillez entrer les commandes suivantes :

- [ ] make
- [ ] ./airWatcher

### Exécuter les tests unitaires

Pour exécuter les tests unitaires des méthodes de notre application, veuillez entrer les commandes suivantes :

- [ ] make
- [ ] ./airWatcher test
