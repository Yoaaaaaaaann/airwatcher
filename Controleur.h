/* -------------------Interface de la classe <Controleur> (fichier Controleur.h)------------------*/
#if ! defined (CONTROLEUR_H)
#define CONTROLEUR_H
//------------------------------------------------------------------------
// Rôle de la classe Controleur
// La classe Controleur est la classe qui fait le lien entre le modèle et la vue. 
// Elle communique directement avec les deux et avec l'utilisateur.
// Globalement, elle reçoit les directives de l'utilisateur pour ensuite appeler les services correspondant et
// ensuite, elle appelle la vue pour affichier ces resultats à l'utilisateur
//------------------------------------------------------------------------
/////////////////////////////////////////////// INCLUDE
//-----------------------------------------Include Système
#include <iostream>
#include <string>
#include <cstring>
#include <ctime>

using namespace std;

class Controleur{
    /////////////////////////////////////////////////// PUBLIC
    //---------------------------------------------Fonctions publiques
    public:
        int main();
        //Mode d'emploi :
        // La fonction main est la fonction qui se lance lors du demarrage de notre programme (si nous ne voulons pas voir les tests)
        // Il s'agit de l'interface utilisateur en ligne de commande.
        int mainTest();
        //Mode d'emploi :
        // La fonction mainTest est la fonction appelée si nous voulons faire des tests. Elle lance une batterie de tests en live afin
        // montrer le bon fonctionnement de nos services
        int obtenirOption(bool&);
        //Mode d'emploi :
        // La fonction obtenirOption renvoie le choix de l'utilsateur sur le service qu'il desire utiliser.
        // La paramètre bool ici permet de savoir si l'utilisateur veut obtenir des informations supplémentaire sur la fonction
        void obtenirGPS(double&, double&);
        //Mode d'emploi :
        // La fonction prend en paramètre deux doubles qui contiendront la longitude et la latitude des coordonnées choisies par l'utilisateur
        tm* obtenirDate();
        //Mode d'emploi :
        //La fonction renvoie un tm* (une date) après l'avoir construit sur la base des informations de l'utilisateur
        int obtenirDuree();
        //Mode d'empploi
        // La fonction renvoie un entier contenant la durée (en heure) d'étude des mesure souhaitée par l'utilisateur 
        double obtenirRayon();
        //Mode d'emploi
        // La fonction renvoie le rayon souhaité par l'utilisateur (en km)
        string obtenirIdCapteur(map<string, Capteur*>*);
        //Mode d'emploi 
        // La fonction renvoie l'ID du capteur souhaité par l'utilisateur. Elle prend en paramètre une liste de capteur afin de vérifier que 
        //le capteur choisi appartient bien a notre liste de capteur.
};

#endif //CONTROLEUR_H
