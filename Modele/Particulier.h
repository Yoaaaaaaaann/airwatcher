//---------- Interface de la classe <Particulier> (fichier Particulier.h) -------------------
#if ! defined (PARTICULIER_H)
#define PARTICULIER_H
/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
#include <string>
#include <vector>
//-------------------------------------------------------- Include personnel
#include "Utilisateur.h"
using namespace std;
//------------------------------------------------------------------------
// Rôle de la classe Particulier
// C'est un modèle qui contient des informations sur Particulier. 
//------------------------------------------------------------------------
class Particulier :public Utilisateur{
///////////////////////////////////////////////////////////////// PUBLIC
    public :
//--------------------------------------------- Fonction publiques
    string getId();
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut idParticulier
    vector<string>* getListeCapteur();
    // Mode d'emploi : Cette fonction permet de renvoyer un vector de l'attribut idCleaner

//------------------------------------------------- Surcharge d'opérateurs
    friend istream & operator >>(istream &,Particulier &);
    // Mode d'emploi : Surcharge d'opérateurs >>
    friend ostream & operator <<(ostream &,Particulier &);
    // Mode d'emploi : Surcharge d'opérateurs <<

//-------------------------------------------- Constructeurs - destructeur
    Particulier(string , string);
    // Mode d'emploi : constructeur
    Particulier(Particulier&);
    // Mode d'emploi : constructeur par copie
    Particulier();
    // Mode d'emploi : constructeur par défaut
    ~Particulier(){}
    // Mode d'emploi : desctructeur
//------------------------------------------------------------------ PRIVE
    private :
//----------------------------------------------------- Attributs privés
    string idParticulier;
    vector<string>* listeIdCapteur = new vector<string>();
};
#endif // PATICULIER_H