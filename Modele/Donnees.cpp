/*---------- Réalisation de la classe <Donnees> (fichier Donnees.cpp) ---------------*/

/////////////////////////////////////////////////////////////////  INCLUDE
//------------------------------------------------------ Include personnel
#include "Donnees.h"
//-------------------------------------------------------- Include système
#include <algorithm>

Donnees:: Donnees(){    
    // Mode d'emploi : constructeur par défaut
}
Donnees :: ~Donnees(){
    // Mode d'emploi : desctructeur
}

void Donnees :: sauvegarderDonnees(string nomFichier){
    // Mode d'emploi : Cette fonction permet de sauvegarder des données, elle prend en argument le nom du fichier a sauvegarder
    ifstream fic;
    fic.open(nomFichier.c_str());
    if(fic){

        string type = nomFichier.substr(13,string::npos);

        if(nomFichier.find("sensors")!= string::npos){
            
            Capteur* capteur;
            while(fic.peek()!=EOF){
                capteur = new Capteur();
                fic>>*capteur;
                listeCapteur.insert(make_pair(capteur->getId(),capteur));
            }
        }
        else if(nomFichier.find("measurements")!= string::npos){
            Mesure* mesure;
            while(fic.peek()!=EOF){
                mesure = new Mesure();
                fic>>*mesure;
                
                listeMesure.insert(make_pair(mesure->getId(),mesure));
            }
        }
        else if(nomFichier.find("cleaners")!= string::npos){
            AirCleaner* airCleaner;
            while(fic.peek()!=EOF){
                airCleaner = new AirCleaner();
                fic>>*airCleaner;
                listeAirCleaner.insert(make_pair(airCleaner->getId(),airCleaner));
            }
        }

        else if(nomFichier.find("attributes")!= string::npos){
            TypeDeMesure* typeMesure;
            while(fic.peek()!=EOF){
                typeMesure = new TypeDeMesure();
                fic>>*typeMesure;
                listeTypeDeMesure.insert(make_pair(typeMesure->getId(),typeMesure));
            }
        }
        else if(nomFichier.find("providers.csv")!= string::npos){
            Fournisseur* fournisseur;
            while(fic.peek()!=EOF){
                fournisseur = new Fournisseur();
                fic>>*fournisseur;
                if(listeFournisseur.find(fournisseur->getId())== listeFournisseur.end()){
                    listeFournisseur.insert(make_pair(fournisseur->getId(),fournisseur));
                }
                else{
                    listeFournisseur.find(fournisseur->getId())->second->getListeIdCleaner()->push_back(fournisseur->getListeIdCleaner()->at(0));
                    delete fournisseur;
                }
            }

        }
        else if(nomFichier.find("users.csv")!= string::npos){
            Particulier* particulier;
            while(fic.peek()!=EOF){
                particulier = new Particulier();
                fic>>*particulier;
                if(listeParticulier.find(particulier->getId())== listeParticulier.end()){
                    listeParticulier.insert(make_pair(particulier->getId(),particulier));
                }
                else{
                    listeParticulier.find(particulier->getId())->second->getListeCapteur()->push_back(particulier->getListeCapteur()->at(0));
                    delete particulier;
                }
            }
        }
        else{
            cerr<<"ERREUR"<<endl;
        }
    }
    else{
        cerr<<"ERREUR d'ouverture du fichier"<<endl;
    }
}
