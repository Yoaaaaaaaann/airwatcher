/*---------- Réalisation de la classe <Service> (fichier Service.cpp) ---------------*/

/////////////////////////////////////////////////////////////////  INCLUDE
using namespace std;
//------------------------------------------------------ Include personnel
#include "Service.h"
#include "Capteur.h"
//-------------------------------------------------------- Include système
#include <cmath>
#include <map>
#include <list>
#include <string>
#include <vector>
#include <ctime>

#define PI 3.14159265359

 
Service::Service(Donnees*donnees){
    // Mode d'emploi :
    // Constructeur de la classe Service. Il prend en paramètre un pointeur sur un objet donné.
    // Rôle d'initialisation des données de l'application.
    d=donnees;
}
 
Service::~Service(){
    // Mode d'emploi :
    // Destructeur de la classe Service.
    // A appeler à la fin de l'utilisation de l'application pour libérer la mémoire.
    if(d!=nullptr) delete d;
}
 
vector<Mesure*> Service:: ChercherMesureParCapteurEtType(string idCapt, string type){
    // Mode d'emploi :
    // Cette méthode prend en paramètre deux chaines de caractères, représentant respectivement
    // un identifiant de capteur et le nom d'un polluant.
    // Elle renvoie un vecteur contenant les (pointeurs sur) Mesures de ce capteur sur le polluant.
    vector<Mesure*> resultat; 
    map<string, Mesure*>::iterator it;
    for(it=d->listeMesure.begin(); it!= d->listeMesure.end(); ++it){
        
        if(it->second->getCapteurId()==idCapt && it->second->getPolluantId()== type){
            resultat.push_back(it->second);
        
        }
    }
    return resultat;
}

vector <string> Service::ChercherCapteurZone(double longi, double lat, int rayon) //rayon en km
{
    // Mode d'emploi :
    // Cette méthode prend en paramètre d'abord deux doubles correspondant à une latitude et à
    // une longitude codant un point, puis un entier correspondant au rayon de la zone.
    // Elle renvoie un vecteur contenant les identifiants des capteurs se trouvant dans la zone
    // décrite par les paramètres.
    vector <string> listId ;
    double dist;
    double longC, latC;
    for( map <string, Capteur*>::iterator it = d->listeCapteur.begin(); it != d->listeCapteur.end(); ++it){
        longC = (it->second)->getLong();
        latC = (it->second)->getLat();
        dist=(acos(cos(lat*PI/180)*cos(latC*PI/180)*cos((longC*PI/180)-(longi*PI/180))+sin(lat*PI/180)*sin(latC*PI/180)))*6371; //rayon terre en km
        if(int(dist)<=rayon)
        {
            listId.push_back((it->first));
        }
    }
    return listId;
}
 
int Service::ConversionIndiceATMO(string type, double concentration)
{
    // Mode d'emploi :
    // Cette méthode prend en paramètre le nom d'un polluant ainsi que la concentration de ce polluant dans l'air.
    // Elle renvoie l'entier correspondant à l'indice ATMO pour ces informations.
    int indice = 0;
    if(type=="O3")
    {
        if(concentration>=0 && concentration<=29) indice = 1;
        else if(concentration>=30 && concentration<=54) indice = 2;
        else if(concentration>=55 && concentration<=79) indice = 3;
        else if(concentration>=80 && concentration<=104) indice = 4;
        else if(concentration>=105 && concentration<=129) indice = 5;
        else if(concentration>=130 && concentration<=149) indice = 6;
        else if(concentration>=150 && concentration<=179) indice = 7;
        else if(concentration>=180 && concentration<=209) indice = 8;
        else if(concentration>=210 && concentration<=239) indice = 9;
        else if(concentration>=240) indice = 10;
    }
    else if(type=="SO2")
    {
        if(concentration>=0 && concentration<=39) indice = 1;
        else if(concentration>=40 && concentration<=79) indice = 2;
        else if(concentration>=80 && concentration<=119) indice = 3;
        else if(concentration>=120 && concentration<=159) indice = 4;
        else if(concentration>=160 && concentration<=199) indice = 5;
        else if(concentration>=200 && concentration<=249) indice = 6;
        else if(concentration>=250 && concentration<=299) indice = 7;
        else if(concentration>=300 && concentration<=399) indice = 8;
        else if(concentration>=400 && concentration<=499) indice = 9;
        else if(concentration>=500) indice = 10;
    }
    else if(type=="NO2")
    {
        if(concentration>=0 && concentration<=29) indice = 1;
        else if(concentration>=30 && concentration<=54) indice = 2;
        else if(concentration>=55 && concentration<=84) indice = 3;
        else if(concentration>=85 && concentration<=109) indice = 4;
        else if(concentration>=110 && concentration<=134) indice = 5;
        else if(concentration>=135 && concentration<=164) indice = 6;
        else if(concentration>=165 && concentration<=199) indice = 7;
        else if(concentration>=200 && concentration<=274) indice = 8;
        else if(concentration>=275 && concentration<=399) indice = 9;
        else if(concentration>=400) indice = 10;
    }
    else if(type=="PM10")
    {
        if(concentration>=0 && concentration<=6) indice = 1;
        else if(concentration>=7 && concentration<=13) indice = 2;
        else if(concentration>=14 && concentration<=20) indice = 3;
        else if(concentration>=21 && concentration<=27) indice = 4;
        else if(concentration>=28 && concentration<=34) indice = 5;
        else if(concentration>=35 && concentration<=41) indice = 6;
        else if(concentration>=42 && concentration<=49) indice = 7;
        else if(concentration>=50 && concentration<=64) indice = 8;
        else if(concentration>=65 && concentration<=79) indice = 9;
        else if(concentration>=80) indice = 10;
    }
    return indice;
}
 
int Service::CalculMoyenneAir(double longi , double lat,double rayon, tm *date, int duree  )
{
    // Mode d'emploi :
    // Cette méthode prend en paramètre une longitude, une latitude, un rayon ainsi qu'une date et une durée.
    // C'est une des fonctionnalités proposées par notre application, permettant de trouver la qualité
    // de l'air (grâce à l'indice ATMO) d'une zone sur une période donnée.
    list <string> type = {"O3","NO2","SO2","PM10"};
    vector <string> listId = ChercherCapteurZone(longi,lat,rayon); // retourne liste de capteurs de cette zone

    vector<Mesure *> listMesures;
    int somme;
    int nombreMesures;
    double concentration;
    int resultat = 0;
    
    for(list<string> :: iterator it1 = type.begin();it1 != type.end();++it1 ) // on parcours la liste des polluants
    {

        nombreMesures =0;
        somme =0;
        vector<string> :: const_iterator it2;
        for(it2 = listId.cbegin(); it2 != listId.cend(); ++it2) // on parcours la liste des capteurs présents dans la zone
        {        
            listMesures = ChercherMesureParCapteurEtType(*it2,*it1);
            vector<Mesure*> :: const_iterator it3;
            for(it3 = listMesures.cbegin(); it3 != listMesures.cend(); ++it3) // on parcours la liste des mesures de chaque capteur retenu
            {

                tm * dateFin = new tm ();

                dateFin-> tm_mday = date->tm_mday;
                dateFin-> tm_hour = date->tm_hour + duree;
         
                dateFin-> tm_min = date->tm_min;
                dateFin-> tm_sec = date->tm_sec;
                dateFin-> tm_year = date->tm_year;
                dateFin-> tm_mon = date->tm_mon;  
                time_t dateFinTime = mktime(dateFin);
                if((difftime(mktime((*it3)->getDate()),mktime(date)) >= 0.0) && (difftime(mktime((*it3)->getDate()),mktime(dateFin))<=0.0))
                {                    nombreMesures++;
                    somme = somme + (*it3)->getConcentration();
                } 
            }

        }
        if(nombreMesures==0) return 0;
        concentration  = somme /  nombreMesures;
        resultat = max(resultat, ConversionIndiceATMO(*it1,concentration));
    }
    return resultat;
}

int Service::DeterminerFiabiliteCapteur(string idCap, tm* dateDeb, int duree)
{
    // Mode d'emploi :
    // Cette méthode prend en paramètre l'identifiant d'un capteur, une date et une durée.
    // C'est une des fonctionnalités proposées par notre application, permettant de récupérer un entier,
    // correspondant à la fiabilité d'un capteur sur une période donnée.
   Capteur* cap= d->getCapteurParId(idCap);
   tm* dateFin = new tm();
   dateFin->tm_hour = dateDeb->tm_hour + duree;
   dateFin->tm_year = dateDeb->tm_year;
   dateFin->tm_mday = dateDeb->tm_mday;
   dateFin->tm_mon = dateDeb->tm_mon;
   dateFin->tm_min = dateDeb->tm_min;
   dateFin->tm_sec = dateDeb->tm_sec;
   time_t deb = mktime(dateDeb);
   time_t fin = mktime(dateFin);
 
   map<string, int> valeurVoisin; 
   vector<Mesure*> mesureCapteur;
   map<string, int> valeurCapteur;
   double concentrationCapteur = 0;
   double concentration=0;
   int nbMesure;
   int nbMesureCapteur;
   double sommeCapteur;
   double somme;
   vector<string> idCapteurs = ChercherCapteurZone(cap->getLong(), cap->getLat(), 100); //vecteur des id des capteurs voisins (dans un rayon de 100km) au capteur dont on veut déterminer la fiabilité
   vector<string> type = {"O3","NO2","SO2","PM10"};
   for(int t = 0; t < type.size(); ++ t){
    nbMesure = 0;
    nbMesureCapteur = 0;
    sommeCapteur = 0;
    somme = 0;

    for(int i = 0; i < idCapteurs.size( ); ++ i){
        vector<Mesure*> mesures = ChercherMesureParCapteurEtType(idCapteurs[i],type[t]);
        time_t time;
        if(idCapteurs[i] != cap->getId()){ //somme des concentration des mesures effectuées par tous les capteurs présents dans la zone du capteur dont on veut déterminer la fiabilité sauf ce dernier
            for(int j = 0; j < mesures.size( ); ++ j){
                time = mktime(mesures[j]->getDate());
                if(difftime(time, deb) >= 0 && difftime(fin,time) >= 0){
                    nbMesure += 1;
                    somme += mesures[j]->getConcentration();
                }
            }
        }
        else{ //somme des concentrations des mesures effectuées par le capteur dont on veut déterminer la fiabilité
            for(int k = 0; k < mesures.size( ); ++ k){
                time = mktime(mesures[k]->getDate());
                if(difftime(time, deb) >= 0 && difftime(fin,time) >= 0){
                    nbMesureCapteur += 1;
                    sommeCapteur += mesures[k]->getConcentration();
                }
            }
        }
    }
      
        if(nbMesure==0)return -1;
      
        concentration = somme/nbMesure; // moyenne des concentrations des mesures effectuées par tous les capteurs présents dans la zone du capteur dont on veut déterminer la fiabilité sauf ce dernier
        
        valeurVoisin.insert(make_pair(type[t], ConversionIndiceATMO(type[t],concentration)));
        concentrationCapteur = sommeCapteur/nbMesureCapteur; // moyenne des concentrations des mesures effectuées par le capteur dont on veut déterminer la fiabilité
        valeurCapteur.insert(make_pair(type[t], ConversionIndiceATMO(type[t],concentrationCapteur)));
    }
    int res=0;
    for(int tt = 0; tt < type.size(); ++ tt){
       res = res + abs(valeurVoisin[type[tt]] - valeurCapteur[type[tt]]); 
    }
    res = min(10,res);

    return 10 - res;
}
