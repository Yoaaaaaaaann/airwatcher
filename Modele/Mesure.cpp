/*---------- Réalisation de la classe <Mesure> (fichier Mesure.cpp) ---------------*/

/////////////////////////////////////////////////////////////////  INCLUDE
//------------------------------------------------------ Include personnel
#include "Mesure.h"
//------------------------------------------------------------- Constantes
int Mesure:: compteMesure =0;

Mesure :: Mesure(string idCapt, string poll, double concent, tm* aDate){
    // Mode d'emploi : constructeur 
    idMesure="Mesure";
    idMesure += to_string(compteMesure);
    compteMesure+=1;

    idCapteur= idCapt;
    idPolluant = poll;
    concentration =concent;
    date = aDate;
    idMesure="";
}
Mesure :: Mesure(){
    // Mode d'emploi : constructeur par défaut
    idMesure="Mesure";
    idMesure += to_string(compteMesure);
    compteMesure+=1;
    idCapteur="";
    idPolluant="";
    concentration=0;
    date = nullptr;
}

Mesure :: ~Mesure(){
    // Mode d'emploi : desctructeur
    if(this->date != nullptr){
        delete this->date;
    }
    
}

istream & operator >>(istream &flux,Mesure &m)
{
    // Mode d'emploi : Surcharge d'opérateurs >>
    string dateStr;
    getline(flux, dateStr, ';');
    tm* dat = new tm();
    dat->tm_mday = stoi(dateStr.substr(8,2));
    dat->tm_mon = stoi(dateStr.substr(5,2))-1;
    dat->tm_year = stoi(dateStr.substr(0,4)) - 1900;
    dat->tm_hour = stoi(dateStr.substr(11,2));
    dat->tm_min = stoi(dateStr.substr(14,2));
    dat->tm_sec = stoi(dateStr.substr(17,2));
    m.date = dat;

    getline(flux, m.idCapteur,';');
    getline(flux, m.idPolluant,';');
    string concentrationStr;
    getline(flux, concentrationStr,';');
    m.concentration = stod(concentrationStr);
    flux.get();
    return flux;
}

ostream & operator <<(ostream &flux,Mesure &m)
{
    // Mode d'emploi : Surcharge d'opérateurs <<
    flux<<"Oui je suis une mesure:"<<endl;
    string dateStr(asctime(m.getDate()));
    flux<<m.getId()<<';'<<dateStr.substr(0,24)<<";"<<m.getCapteurId()<<';'<<m.getPolluantId()<<';'<<m.getConcentration()<<';'<<endl;
    return flux;
}
string Mesure:: getId(){
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut idMesure
    return this->idMesure;
}
string Mesure :: getCapteurId(){
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut idCapteur
    return this->idCapteur;
}
string Mesure :: getPolluantId(){
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut idPolluant
    return this->idPolluant;
}
double Mesure :: getConcentration(){
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut concentration
    return this->concentration;
}
tm* Mesure:: getDate(){
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut getDate
    return this->date;
}
