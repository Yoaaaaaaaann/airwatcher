//---------- Interface de la classe <Capteur> (fichier Capteur.h) -------------------
#if ! defined (CAPTEUR_H)
#define CAPTEUR_H
/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
#include <string>
using namespace std;
//------------------------------------------------------------------------
// Rôle de la classe Capteur
// C'est un modèle qui contient des informations sur Capteur. 
//------------------------------------------------------------------------
class Capteur{
///////////////////////////////////////////////////////////////// PUBLIC
    public :
    //--------------------------------------------- Fonction publiques
    string getId();
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut idCapteur
    double getLong();
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut longitude
    double getLat();
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut latitude

//------------------------------------------------- Surcharge d'opérateurs
    friend istream & operator >>(istream &,Capteur &);
    // Mode d'emploi : Surcharge d'opérateurs >>
    friend ostream & operator <<(ostream &,Capteur &);
    // Mode d'emploi : Surcharge d'opérateurs <<
//-------------------------------------------- Constructeurs - destructeur
    Capteur(string , double, double);
// Mode d'emploi : constructeur
    Capteur(Capteur&);
// Mode d'emploi : constructeur par copie
    Capteur();
// Mode d'emploi : constructeur par défaut
    ~Capteur(){}
// Mode d'emploi : desctructeur

//------------------------------------------------------------------ PRIVE
    private :
//----------------------------------------------------- Attributs privés
    string idCapteur;
    double latitude;
    double longitude;
};
#endif //CAPTEUR_H