//---------- Interface de la classe <TypeDeMesure> (fichier TypeDeMesure.h) -------------------
#if ! defined (TYPEDEMESURE_H)
#define TYPEDEMESURE_H
/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
#include <string>
using namespace std;
//------------------------------------------------------------------------
// Rôle de la classe TypeDeMesure
// C'est un modèle qui contient des informations sur TypeDeMesure. 
//------------------------------------------------------------------------
class TypeDeMesure{
///////////////////////////////////////////////////////////////// PUBLIC
    public :
//--------------------------------------------- Fonction publiques
    string getId();
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut idPolluant
    string getUnite();
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut unite
    string getDescritpion();
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut description

//------------------------------------------------- Surcharge d'opérateurs
    friend istream & operator >>(istream &,TypeDeMesure &);
    // Mode d'emploi : Surcharge d'opérateurs >>
    friend ostream & operator <<(ostream &,TypeDeMesure &);
    // Mode d'emploi : Surcharge d'opérateurs <<

//-------------------------------------------- Constructeurs - destructeur
    TypeDeMesure(string , string, string);
    // Mode d'emploi : constructeur
    TypeDeMesure(TypeDeMesure&);
    // Mode d'emploi : constructeur par copie
    TypeDeMesure();
    // Mode d'emploi : constructeur par défaut
    ~TypeDeMesure(){}
    // Mode d'emploi : desctructeur
//------------------------------------------------------------------ PRIVE
    private :
//----------------------------------------------------- Attributs privés
    string idPolluant;
    string unite;
    string description;
};
#endif // TYPEDEMESURE_H