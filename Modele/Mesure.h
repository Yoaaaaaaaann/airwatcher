//---------- Interface de la classe <Mesue> (fichier Mesure.h) -------------------
#if ! defined (MESURE_H)
#define MESURE_H
/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
#include <string>
#include <cstring>
#include <ctime>
using namespace std;
//------------------------------------------------------------------------
// Rôle de la classe Mesure
// C'est un modèle qui contient des informations sur Mesure. 
//------------------------------------------------------------------------
class Mesure{
///////////////////////////////////////////////////////////////// PUBLIC
    public :
//--------------------------------------------- Fonction publiques
    tm* getDate();
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut date
    double getConcentration();
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut concentration
    string getId();
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut idMesure
    string getCapteurId();
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut idCapteur
    string getPolluantId();
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut idPolluant

//------------------------------------------------- Surcharge d'opérateurs
    friend istream & operator >>(istream &,Mesure &);
    // Mode d'emploi : Surcharge d'opérateurs >>
    friend ostream & operator <<(ostream &,Mesure &);
    // Mode d'emploi : Surcharge d'opérateurs <<
//-------------------------------------------- Constructeurs - destructeur
    Mesure(string, string, double,tm* );
    // Mode d'emploi : constructeur
    Mesure(Mesure&);
    // Mode d'emploi : constructeur par copie
    Mesure();
    // Mode d'emploi : constructeur par défaut
    ~Mesure();
    // Mode d'emploi : desctructeur
//------------------------------------------------------------------ PRIVE
    private :
//----------------------------------------------------- Attributs privés
    static int compteMesure;
    string idMesure;
    double concentration;
    string idCapteur;
    string idPolluant;
    tm *date;
};
#endif //MESURE_H
