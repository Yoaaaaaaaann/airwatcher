/* -------------------Interface de la classe <Service> (fichier Service.h)------------------*/
#if ! defined (SERVICE_H)
#define SERVICE_H

//--------------------------------------------------- Interfaces utilisées
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "Donnees.h"

using namespace std;

//------------------------------------------------------------------------
// Rôle de la classe Service :
// La classe service implémente toutes les fonctionnalités proposées par notre application.
// Elle fait partie du Modèle, et possède toutes les données de l'application.
//------------------------------------------------------------------------

class Service{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    Service(Donnees*);
    // Mode d'emploi :
    // Constructeur de la classe Service. Il prend en paramètre un pointeur sur un objet donné.
    // Rôle d'initialisation des données de l'application.

    ~Service();
    // Mode d'emploi :
    // Destructeur de la classe Service.
    // A appeler à la fin de l'utilisation de l'application pour libérer la mémoire.

    vector <string> ChercherCapteurZone(double, double, int);
    // Mode d'emploi :
    // Cette méthode prend en paramètre d'abord deux doubles correspondant à une latitude et à
    // une longitude codant un point, puis un entier correspondant au rayon de la zone.
    // Elle renvoie un vecteur contenant les identifiants des capteurs se trouvant dans la zone
    // décrite par les paramètres.

    vector <Mesure*> ChercherMesureParCapteurEtType(string, string);
    // Mode d'emploi :
    // Cette méthode prend en paramètre deux chaines de caractères, représentant respectivement
    // un identifiant de capteur et le nom d'un polluant.
    // Elle renvoie un vecteur contenant les (pointeurs sur) Mesures de ce capteur sur le polluant.

    int CalculMoyenneAir(double longi , double lati, double rayon, tm * date, int duree);
    // Mode d'emploi :
    // Cette méthode prend en paramètre une longitude, une latitude, un rayon ainsi qu'une date et une durée.
    // C'est une des fonctionnalités proposées par notre application, permettant de trouver la qualité
    // de l'air (grâce à l'indice ATMO) d'une zone sur une période donnée.

    int DeterminerFiabiliteCapteur(string idcap, tm* dateDeb, int duree);
    // Mode d'emploi :
    // Cette méthode prend en paramètre l'identifiant d'un capteur, une date et une durée.
    // C'est une des fonctionnalités proposées par notre application, permettant de récupérer un entier,
    // correspondant à la fiabilité d'un capteur sur une période donnée.

    int ConversionIndiceATMO(string, double);
    // Mode d'emploi :
    // Cette méthode prend en paramètre le nom d'un polluant ainsi que la concentration de ce polluant dans l'air.
    // Elle renvoie l'entier correspondant à l'indice ATMO pour ces informations.

private:
        Donnees *d;
        // d est un pointeur sur un objet Donnees, contenant toutes informations des mesures,
        // des capteurs, des airCleaners, des fournisseurs, des particuliers et des polluants.
        
};

#endif //SERVICE_H