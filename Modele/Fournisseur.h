//---------- Interface de la classe <Fournisseur> (fichier Fournisseur.h) -------------------
#if ! defined (FOURNISSEUR_H)
#define FOURNISSEUR_H
/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
#include <string>
#include <vector>
//-------------------------------------------------------- Include personnel
#include "Utilisateur.h"
using namespace std;
//------------------------------------------------------------------------
// Rôle de la classe Founisseur
// C'est un modèle qui contient des informations sur Fournisseur. 
//------------------------------------------------------------------------
class Fournisseur :public Utilisateur{
///////////////////////////////////////////////////////////////// PUBLIC
    public :
//--------------------------------------------- Fonction publiques
    string getId();
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut idFournisseur
    vector<string>* getListeIdCleaner();
    // Mode d'emploi : Cette fonction permet de renvoyer un vector de l'attribut idCleaner

//------------------------------------------------- Surcharge d'opérateurs
    friend istream & operator >>(istream &,Fournisseur &);
    // Mode d'emploi : Surcharge d'opérateurs >>
    friend ostream & operator <<(ostream &,Fournisseur &);
    // Mode d'emploi : Surcharge d'opérateurs <<
//-------------------------------------------- Constructeurs - destructeur
    Fournisseur(string , string);
    // Mode d'emploi : constructeur
    Fournisseur(Fournisseur&);
    // Mode d'emploi : constructeur par copie
    Fournisseur();
    // Mode d'emploi : constructeur par défaut
    ~Fournisseur(){}
    // Mode d'emploi : desctructeur
//------------------------------------------------------------------ PRIVE
    private :
//----------------------------------------------------- Attributs privés
    string idFournisseur;
    vector<string>* listeIdCleaner = new vector<string>();
};
#endif //FOURNISSEUR_H