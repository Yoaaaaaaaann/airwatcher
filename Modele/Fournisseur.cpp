/*---------- Réalisation de la classe <Fournisseur> (fichier Fournisseur.cpp) ---------------*/

/////////////////////////////////////////////////////////////////  INCLUDE
//------------------------------------------------------ Include personnel
#include "Fournisseur.h"

Fournisseur :: Fournisseur(string idF, string idC){
    // Mode d'emploi : constructeur
    idFournisseur = idF;
    listeIdCleaner->push_back(idC);
}
Fournisseur :: Fournisseur()
:Utilisateur()
{
    // Mode d'emploi : constructeur par défaut
    idFournisseur ="";
}

istream & operator >>(istream &flux,Fournisseur &f)
{
    // Mode d'emploi : Surcharge d'opérateurs >>
    getline(flux, f.idFournisseur,';');
    string idCleaner;
    getline(flux, idCleaner,';');
    f.listeIdCleaner->push_back(idCleaner);
    flux.get();
    return flux;
}

ostream & operator <<(ostream &flux,Fournisseur &f)
{
    // Mode d'emploi : Surcharge d'opérateurs <<
    flux<<"Oui je suis un fournisseur:"<<endl;
    flux<<f.getId()<<";";
    for(int i=0; i<f.listeIdCleaner->size(); i++){
        flux<<f.listeIdCleaner->at(i)<<";";
    }
    flux<<endl;
    return flux;
}

string Fournisseur :: getId(){
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut idFournisseur
    return this->idFournisseur;
}
vector<string>* Fournisseur :: getListeIdCleaner(){
    // Mode d'emploi : Cette fonction permet de renvoyer un vector de l'attribut idCleaner
    return this->listeIdCleaner;
}