/*---------- Réalisation de la classe <TypeDeMesure> (fichier TypeDeMesure.cpp) ---------------*/

/////////////////////////////////////////////////////////////////  INCLUDE
//------------------------------------------------------ Include personnel
#include "TypeDeMesure.h"

TypeDeMesure :: TypeDeMesure(string idP, string unit, string desc){
    // Mode d'emploi : constructeur 
    idPolluant = idP;
    unite = unit;
    description = desc;
    
}
TypeDeMesure :: TypeDeMesure(){
    // Mode d'emploi : constructeur par défaut
    idPolluant = "";
    unite = "";
    description = "";
}

istream & operator >>(istream &flux,TypeDeMesure &tdm)
{
    // Mode d'emploi : Surcharge d'opérateurs >>
    getline(flux, tdm.idPolluant,';');
    getline(flux, tdm.unite,';');
    getline(flux, tdm.description,';');
    flux.get();
    return flux;
}

ostream & operator <<(ostream &flux,TypeDeMesure &tdm)
{
    // Mode d'emploi : Surcharge d'opérateurs <<
    flux<<"Oui je suis un Type de mesure:"<<endl;
    flux<<tdm.idPolluant<<";"<<tdm.unite<<";"<<tdm.description<<";"<<endl;
    return flux;
}

string TypeDeMesure :: getId(){
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut idPolluant
    return this->idPolluant;
}
string TypeDeMesure :: getUnite(){
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut unite
    return this->unite;
}
string TypeDeMesure :: getDescritpion(){
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut description
    return this->description;
}
