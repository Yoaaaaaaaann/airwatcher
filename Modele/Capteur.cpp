/*---------- Réalisation de la classe <Capteur> (fichier Capteur.cpp) ---------------*/

/////////////////////////////////////////////////////////////////  INCLUDE
//------------------------------------------------------ Include personnel
#include "Capteur.h"

Capteur :: Capteur(string id, double lat, double longi){
    // Mode d'emploi : constructeur
    idCapteur = id;
    latitude = lat;
    longitude =longi;
}
Capteur :: Capteur(){
    // Mode d'emploi : constructeur par défaut
    idCapteur="";
    latitude=0;
    longitude=0;
}

istream & operator >>(istream &flux,Capteur &c)
{
    // Mode d'emploi : Surcharge d'opérateurs >>
    getline(flux, c.idCapteur,';');
    string lati;
    getline(flux, lati,';');
    c.latitude = stod(lati);
    string longi;
    getline(flux, longi,';');
    c.longitude = stod(longi);
    flux.get();
    return flux;
}

ostream & operator <<(ostream &flux,Capteur &c)
{
    // Mode d'emploi : Surcharge d'opérateurs <<
    flux<<"Oui je suis un capteur:"<<endl;
    flux<<c.getId()<<";"<<c.getLat()<<';'<<c.getLong()<<';'<<endl;
    return flux;
}

string Capteur :: getId(){
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut idCapteur
    return this->idCapteur;
}
double Capteur :: getLong(){
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut longitude
    return this->longitude;
}
double Capteur :: getLat(){
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut latitude
    return this->latitude;
}