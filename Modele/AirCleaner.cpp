/*---------- Réalisation de la classe <AirCleaner> (fichier AirCleaner.cpp) ---------------*/

/////////////////////////////////////////////////////////////////  INCLUDE
//------------------------------------------------------ Include personnel
#include "AirCleaner.h"

AirCleaner :: AirCleaner(string id, double lat, double longi, tm* tpsDeb, tm* tpsFin){
    // Mode d'emploi : constructeur
    idCleaner= id;
    latitude = lat;
    longitude =longi;
    tempsDebut = tpsDeb;
    tempsFin = tpsFin;
}

AirCleaner :: AirCleaner(){
    // Mode d'emploi : constructeur par défaut
    idCleaner="";
    latitude=0;
    longitude=0;
    tempsDebut = nullptr;
    tempsFin = nullptr;
}

AirCleaner :: ~AirCleaner(){
    // Mode d'emploi : desctructeur
    if(this->tempsDebut != nullptr){
        delete this->tempsDebut;
    }
    if(this->tempsFin != nullptr){
        delete this->tempsFin;
    }
}

istream & operator >>(istream &flux,AirCleaner &c)
{
    // Mode d'emploi : Surcharge d'opérateurs >>
    getline(flux, c.idCleaner,';');
    string longi;
    getline(flux, longi,';');
    c.longitude = stod(longi);
    string lat;
    getline(flux, lat,';');
    c.latitude = stod(lat);
    string tpsDebut;
    getline(flux, tpsDebut, ';');
    tm* td = new tm();
    td->tm_mday = stoi(tpsDebut.substr(8,2));
    td->tm_mon = stoi(tpsDebut.substr(5,2))-1;
    td->tm_year = stoi(tpsDebut.substr(0,4)) - 1900;
    td->tm_hour = stoi(tpsDebut.substr(11,2));
    td->tm_min = stoi(tpsDebut.substr(14,2));
    td->tm_sec = stoi(tpsDebut.substr(17,2));
    c.tempsDebut = td;

    string tpsFin;
    getline(flux, tpsFin, ';');
    tm* tf = new tm();
    tf->tm_mday = stoi(tpsFin.substr(8,2));
    tf->tm_mon = stoi(tpsFin.substr(5,2))-1;
    tf->tm_year = stoi(tpsFin.substr(0,4)) - 1900;
    tf->tm_hour = stoi(tpsFin.substr(11,2));
    tf->tm_min = stoi(tpsFin.substr(14,2));
    tf->tm_sec = stoi(tpsFin.substr(17,2));
    c.tempsFin = tf;
    flux.get();
    return flux;
}

ostream & operator <<(ostream &flux,AirCleaner &c)
{
    // Mode d'emploi : Surcharge d'opérateurs <<
    flux<<"Oui je suis un cleaner:"<<endl;
    string tempsDeb(asctime(c.getTempsDebut()));
    string tempsFin(asctime(c.getTempsFin()));
    flux<<c.getId()<<";"<<c.getLong()<<';'<<c.getLat()<<';'<<tempsDeb.substr(0,24)<<';'<<tempsFin.substr(0,24)<<';'<<endl;
    return flux;
}

string AirCleaner :: getId(){
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut idCleaner
    return this->idCleaner;
}
double AirCleaner :: getLong(){
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut longitude
    return this->longitude;
}
double AirCleaner :: getLat(){
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut latitude
    return this->latitude;
}
tm* AirCleaner:: getTempsDebut(){
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut tempsDebut
    return this->tempsDebut;
}
tm* AirCleaner:: getTempsFin(){
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut tempsFin
    return this->tempsFin;
}