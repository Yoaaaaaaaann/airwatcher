//---------- Interface de la classe <Utilisateur> (fichier Utilisateur.h) -------------------
#if ! defined (UTILISATEUR_H)
#define UTILISATEUR_H
/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;
//------------------------------------------------------------------------
// Rôle de la classe Utilisateur
// C'est un modèle qui contient des informations sur UTILISATEUR. 
//------------------------------------------------------------------------
class Utilisateur{
///////////////////////////////////////////////////////////////// PUBLIC
    public :
//-------------------------------------------- Constructeurs - destructeur
    Utilisateur(){};
    // Mode d'emploi : constructeur par défaut
//------------------------------------------------------------------ PRIVE
    private :
};
#endif // UTILISATEUR_H