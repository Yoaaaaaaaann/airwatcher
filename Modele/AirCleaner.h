//---------- Interface de la classe <AirCleaner> (fichier AirCleaner.h) -------------------
#if ! defined (AIRCLEANER_H)
#define AIRCLEANER_H
/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
#include <string>
#include <cstring>
#include <ctime>
using namespace std;
//------------------------------------------------------------------------
// Rôle de la classe AirCleaner
// C'est un modèle qui contient des informations sur AirCleaner. 
//------------------------------------------------------------------------
class AirCleaner{
///////////////////////////////////////////////////////////////// PUBLIC
    public :
    //--------------------------------------------- Fonction publiques
    string getId();
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut idCleaner
    double getLong();
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut longitude
    double getLat();
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut latitude
    tm* getTempsDebut();
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut tempsDebut
    tm* getTempsFin();
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut tempsFin

//------------------------------------------------- Surcharge d'opérateurs
    friend istream & operator >>(istream &,AirCleaner &);
    // Mode d'emploi : Surcharge d'opérateurs >>
    friend ostream & operator <<(ostream &,AirCleaner &);
    // Mode d'emploi : Surcharge d'opérateurs <<
//-------------------------------------------- Constructeurs - destructeur
    AirCleaner(string , double, double, tm*, tm*);
    // Mode d'emploi : constructeur
    AirCleaner(AirCleaner&);
    // Mode d'emploi : constructeur par copie
    AirCleaner();
    // Mode d'emploi : constructeur par défaut
    ~AirCleaner();
    // Mode d'emploi : desctructeur

//------------------------------------------------------------------ PRIVE
    private :
//----------------------------------------------------- Attributs privés
    string idCleaner;
    double latitude;
    double longitude;
    tm *tempsDebut;
    tm *tempsFin;
};
#endif // AIRCLEANER_H