/*---------- Réalisation de la classe <Particulier> (fichier Particulier.cpp) ---------------*/

/////////////////////////////////////////////////////////////////  INCLUDE
//------------------------------------------------------ Include personnel
#include "Particulier.h"

Particulier :: Particulier(string idF, string idC){
    // Mode d'emploi : constructeur 
    idParticulier = idF;
    listeIdCapteur->push_back(idC);

}
Particulier :: Particulier()
: Utilisateur()
{
    // Mode d'emploi : constructeur par défaut
    idParticulier="";
   
}

istream & operator >>(istream &flux,Particulier &u)
{
    // Mode d'emploi : Surcharge d'opérateurs >>
    getline(flux, u.idParticulier,';');
    string idCapteur;
    getline(flux, idCapteur,';');
    u.listeIdCapteur->push_back(idCapteur);
    flux.get();
    return flux;
}

ostream & operator <<(ostream &flux,Particulier &u)
{
    // Mode d'emploi : Surcharge d'opérateurs <<
    flux<<"Oui je suis un Particulier:"<<endl;
    flux<<u.getId()<<";";
    for(int i=0; i<u.listeIdCapteur->size(); i++){
        flux<<u.listeIdCapteur->at(i)<<";";
    }
    flux<<endl;
    
    return flux;
}

string Particulier :: getId(){
    // Mode d'emploi : Cette fonction permet de renvoyer l'attribut idParticulier
    return this->idParticulier;
}
vector<string>* Particulier :: getListeCapteur(){
    // Mode d'emploi : Cette fonction permet de renvoyer un vector de l'attribut idCleaner
    return this->listeIdCapteur;
}