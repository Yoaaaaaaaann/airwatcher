//---------- Interface de la classe <Donnees> (fichier Donnees.h) -------------------
#if ! defined (DONNEES_H)
#define DONNEES_H
/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
#include <string>
#include <map>
#include <algorithm>
#include <fstream>
//-------------------------------------------------------- Include personnel
#include "AirCleaner.h"
#include "Capteur.h"
#include "Mesure.h"
#include "Fournisseur.h"
#include "Particulier.h"
#include "TypeDeMesure.h"
using namespace std;
//------------------------------------------------------------------------
// Rôle de la classe Données
// C'est un modèle qui contient des informations sur Données. 
//------------------------------------------------------------------------
class Donnees{
///////////////////////////////////////////////////////////////// PUBLIC
    public:
    friend class Service;
    //--------------------------------------------- Fonction publiques
    void sauvegarderDonnees(string);
    // Mode d'emploi : Cette fonction permet de sauvegarder des données
    Capteur*getCapteurParId(string id){
        return listeCapteur[id];
    }
    // Mode d'emploi : Cette fonction permet de renvoyer un capteur par l'attribut idCapteur
    Mesure*getMesureParId(string id){
        return listeMesure[id];
    }
    // Mode d'emploi : Cette fonction permet de renvoyer un mesure par l'attribut idMesure
    Fournisseur*getFournisseurParId(string id){
        return listeFournisseur[id];
    }
    // Mode d'emploi : Cette fonction permet de renvoyer un fournisseur par l'attribut idFournisseur
    Particulier*getParticulierParId(string id){
        return listeParticulier[id];
    }
    // Mode d'emploi : Cette fonction permet de renvoyer un particulier par l'attribut idParticulier
    AirCleaner*getAirCleanerParId(string id){
        return listeAirCleaner[id];
    }
    // Mode d'emploi : Cette fonction permet de renvoyer un aircleaner par l'attribut idAirCleaner
    TypeDeMesure*getTypeDeMesureParId(string id){
        return listeTypeDeMesure[id];
    }
    // Mode d'emploi : Cette fonction permet de renvoyer un type de mesure par l'attribut idTypeDeMesure
    map<string, Capteur*>* getListeCapteur(){
        return &listeCapteur;
    }
    // Mode d'emploi : Cette fonction permet de renvoyer une liste de capteur avec l'attribut idCapteur
//-------------------------------------------- Constructeurs - destructeur
    Donnees();
    // Mode d'emploi : constructeur par défaut
    ~Donnees();
    // Mode d'emploi : desctructeur

//------------------------------------------------------------------ PRIVE
   private:
//----------------------------------------------------- Attributs privés
    map<string, Mesure*> listeMesure;
    map<string, Capteur*> listeCapteur;
    map<string, AirCleaner*> listeAirCleaner;
    map<string, Fournisseur*> listeFournisseur;
    map<string, Particulier*> listeParticulier;
    map<string, TypeDeMesure*> listeTypeDeMesure;
};
#endif //DONNEES_H