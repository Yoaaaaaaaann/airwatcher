//---------- Interface de la classe <Vue> (fichier Vue.h) -------------------
#if ! defined(VUE_H)
#define VUE_H

#include <iostream>
using namespace std;

//------------------------------------------------------------------------
// Rôle de la classe Vue
// La classe Vue est la classe qui permet de communiquer des informations à l'utilisateur. 
// Elle se contente de recupérer des données du controleur et de les afficher sur la console
//------------------------------------------------------------------------
class Vue{

///////////////////////////////////////////////////////////////// PUBLIC
    public:
    //--------------------------------------------- Fonction publiques
    void afficherBienvenue();
    // Mode d'emploi :
    // Cette fonction permet d'affichier Bienvenue à l'utilisateur
    void afficherOptions();
    // Mode d'emploi :
    // Cette fonction permet d'afficher les options qui s'offrent à l'utilisateur
    void afficherMoyenneAir(int indiceATMO, double longi, double lati, double rayon, tm*date, int duree);
    // Mode d'emploi :
    // Cette fonction prend en parametre toutes les caractéristiques des mesures pour afficher le resultat à l'utilisateur
    void afficherFiabiliteCapteur(int indiceFiabilite, string idCapteur, tm*date, int duree);
    // Mode d'emploi :
    // Cette fonction prend en parametre toutes les caractéristiques des mesures pour afficher le resultat à l'utilisateur
    void informationCalculMoyennAir();
    // Mode d'emploi :
    // Cette fonction affiche des informations supplémentaires sur la fonction CalculMoyenneAir
    void informationFiabiliteCapteur();
    // Mode d'emploi :
    // Cette fonction affichier des informations supplémentaire sur la fonction FiabiliteCapteur
    void demanderGPS();
    // Mode d'emploi :
    // Cette fonction affiche un message de demande de coordonnées GPS
    void demanderDate();
    // Mode d'emploi :
    // Cette fonction affiche un message de demande de Date
    void demanderDuree();
    // Mode d'emploi :
    // Cette fonction affiche un message de demande de durée
    void demanderIdCapteur();
    // Mode d'emploi :
    // Cette fonction affiche un message de l'ID d'un capteur
    void demanderRayon();
    // Mode d'emploi :
    // Cette fonction affiche un message de demande de rayon
    void serviceNonDisponible();
    // Mode d'emploi :
    // Cette fonction affiche un message indiquant que le service choisi n'a pas été developpée
    void quitterApp();
    // Mode d'emploi :
    // Cette fonction affiche un message d'au revoir
    void afficherComplexite();
    // Mode d'emploi :
    // Cette fonction affiche les complexité de nos algorithmes
};


#endif // VUE_H
